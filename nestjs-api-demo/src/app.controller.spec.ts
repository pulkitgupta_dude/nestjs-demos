import { HttpModule, HttpService } from '@nestjs/axios';
import { HttpException, HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { User } from './user';
import { UserController } from './user.controller';
import { UserRequest } from './user.request';
import { UserService } from './user.service';


describe('UserController', () => {
  let userController: UserController;
  let userService: UserService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports:[HttpModule],
      controllers: [UserController],
      providers: [UserService],
    }).compile();

    userController = app.get<UserController>(UserController);
    userService = app.get<UserService>(UserService);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      jest.spyOn(userService, 'getHello').mockImplementation(() => "test");
      expect(userController.getHello()).toBe('test');
    });
  });

  describe('getUsers', () => {
    it('should return 1 user', async () => 
    {
      const result:User[] = [{"_id":"xxxyyy-002", "name":"Pulkit Gupta", "password":"ppp", "email":"p.g@g.com"}];
      jest.spyOn(userService, 'getUsers').mockImplementation(() => {
        return Promise.resolve(result);
     });

     jest.spyOn(userService, 'getUsers').mockImplementation(() => {
      return Promise.resolve(result);
      });
      expect(await userController.getUsers()).toEqual(result);
    });
  });

  describe('createUser', () => {
    it('should create 1 user', async () => 
    {
      const result:User = {"_id":"xxxyyy-002", "email":"p.g@g.com"};
      const request:UserRequest = {"email":"pulkit.gupta@g.com", "password":"password"};

      jest.spyOn(userService, 'createUser').mockImplementation(() => {
        return Promise.resolve(result);
     });

      expect(await userController.createUser(request)).toEqual(result);
    });

    it('should throw 400 Bad Request', async () => 
    {
      const request:any = {"email":"pulkit.gupta@g.com"};
     await userController.createUser(request).then().catch((error) => {
      expect(error.status).toBe(400);
      expect(error.message).toBe("Email or password cannot be blank");
    });   

    });

  });


});
