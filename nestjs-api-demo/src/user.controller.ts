import { Body, Controller, Get, Header, HttpCode, HttpException, HttpStatus, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from './auth.guard';
import { User } from './user';
import { UserRequest } from './user.request';
import { UserService } from './user.service';

@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  getHello(): string {
    return this.userService.getHello();
  }

  @UseGuards(AuthGuard)
  @Get("/users")
  async getUsers(): Promise<User[]> {
    return this.userService.getUsers();
  }

  @UseGuards(AuthGuard)
  @HttpCode(201)
  @Post("/user")
  async createUser(@Body() userRequest: UserRequest): Promise<User> {
    if(!userRequest.email || !userRequest.password)
    {
      throw new HttpException('Email or password cannot be blank', HttpStatus.BAD_REQUEST);
    }    
    return this.userService.createUser(userRequest);
  }
}
