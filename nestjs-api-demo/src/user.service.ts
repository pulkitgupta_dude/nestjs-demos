import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { firstValueFrom, Observable } from 'rxjs';
import { User } from './user';
import { UserRequest } from './user.request';

@Injectable()
export class UserService {
  constructor(private httpService: HttpService) {}

  getHello(): string {
    return 'Hello World!';
  }
  public async getUsers():Promise<User[]>
  {
     let response =  await  firstValueFrom(this.httpService.get('http://localhost:4444/api/users'));
     return response.data;    
  }

  public async createUser(user:UserRequest):Promise<User>
  {
     let response =  await  firstValueFrom(this.httpService.post('http://localhost:4444/api/users/register', user));
     return response.data.data;    
  }


}
