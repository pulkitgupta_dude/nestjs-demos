import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean | Promise<boolean>
                                                  | Observable<boolean> 
  {
    const request = context.switchToHttp().getRequest();    
    let userAgent = request.headers["user-agent"];
    console.log("Inside Guard: ", userAgent);
    return userAgent && userAgent.indexOf("Chrome") > -1;
  }
}