# README #

This repo is a monorepo containing several repos with examples on NestJs implementations.

### Install Node and NestJs CLI ###
 
* Install `npm`
* Run `npm i -g @nestjs/cli`

### Install MongoDB ###
`docker run -p 27017:27017 --name mongo mongo:latest`

### Install dependencies ###
Go into each folder and:
    run `npm i`

### Running demos ###
Go into each folder and:
    run `npm run start:dev`

### Who do I talk to? ###
Pulkit Gupta (pulkit.gupta@dudesolutions.com)

 
