import { IsString, IsNotEmpty, IsDate } from 'class-validator';
import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';

export class CreateBookDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  title: string;

  @ApiProperty()
  @IsNotEmpty()
  authorId: any;

  @ApiProperty()
  @IsNotEmpty()
  genreId: any;

  @ApiProperty()
  @IsDate()
  publishDate: Date;
}

export class UpdateBookDto extends PartialType(CreateBookDto) {}
