import { ApiProperty } from '@nestjs/swagger';
import { Book } from '../entities';

export class CreateBookResponseDto {
  @ApiProperty()
  success: boolean;
  
  @ApiProperty()
  createdBook: Book;
}
