import { Controller, Get, Param, Post, Body, Put, UseGuards, Inject, CACHE_MANAGER, UseInterceptors, CacheInterceptor, ParseIntPipe } from '@nestjs/common';
import { ApiParam } from '@nestjs/swagger';
import { Cache } from 'cache-manager';
import { AuthGuard } from 'src/authorizer/auth.guard';
import { ForbiddenException } from 'src/exceptions/forbidden-exception';
import { CreateAuthorDto, UpdateAuthorDto } from '../core/dtos';
import { AuthorServices } from '../services/use-cases/author/author-services.service';

@UseGuards(AuthGuard)
@UseInterceptors(CacheInterceptor)
@Controller('api/author')
export class AuthorController {
  constructor(private authorServices: AuthorServices, @Inject(CACHE_MANAGER) private cacheManager: Cache) {}
  

  @Get()
  async getAll() {
    return this.authorServices.getAllAuthors();
  }
  @Get(':id')
  @ApiParam({name: 'id', required: true, description: 'Id of the author', schema: { oneOf: [{type: 'string'}, {type: 'integer'}]}})
  async getById(@Param('id') id: any) {
    return this.authorServices.getAuthorById(id);
  }

  @Get('demo/:id')
  @ApiParam({name: 'id', required: true, description: 'Id of the author', schema: { oneOf: [{type: 'integer'}]}})
  async getById2(@Param('id', ParseIntPipe) id: any) {
    return new ForbiddenException();;
  }

  @Post()
  createAuthor(@Body() authorDto: CreateAuthorDto) {
    return this.authorServices.createAuthor(authorDto);
  }

  @Put(':id')
  updateAuthor(
    @Param('id') authorId: string,
    @Body() updateAuthorDto: UpdateAuthorDto,
  ) {
    return this.authorServices.updateAuthor(authorId, updateAuthorDto);
  }
}
